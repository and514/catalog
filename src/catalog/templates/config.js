var config = {
    url: {
        {% for url_key, url_value in urls.items %}
        "{{ url_key }}": "{{ url_value }}",
        {% endfor %}
    },
    PositionPerson: {
        {% for k, v in PositionPerson.items %}
        "{{ k }}": "{{v}}",
        {% endfor %}
    },
    StatusMovieList: [
        {% for k, v in StatusMovie.items %}
            {"id": "{{ k }}", "name": "{{v}}"},
        {% endfor %}
    ],
    StatusMovieDict: {
        {% for k, v in StatusMovie.items %}
            "{{ k }}": "{{v}}",
        {% endfor %}
    },
    StatusMovieKeyNames: {
        {% for k, v in StatusMovieKeyNames.items %}
        "{{ k }}": "{{v}}",
        {% endfor %}
    },
};
