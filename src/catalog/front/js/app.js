// app.js
var catalogApp = angular.module('catalogApp', ['ui.router', 'ngCookies', 'xeditable', 'ui.bootstrap', 'ngSanitize']);


catalogApp.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider) {

    $urlRouterProvider.otherwise('/films');

    $stateProvider
    .state('films', {
        url: '/films',
        templateUrl: 'partials/films/index.html',
        controller: films_controller
    })
    //.state('films.list', {
    //    url: '/list',
    //    templateUrl: 'partials/films/list.html',
    //    controller: film_list_controller
    //  //views: {
    //  //  "viewA": { template: "index.viewA" },
    //  //  "viewB": { template: "index.viewB" }
    //  //}
    //})
    ;

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';

}]);


catalogApp.run(function($rootScope, $http, $cookies){
  $http.defaults.headers.common["X-CSRFToken"] = $cookies.get('csrftoken');
  return $rootScope.csrftoken = $cookies.get('csrftoken');
});


function err_msg($scope, msg){
      $scope.message = msg;
      $scope.error = true;
}


function films_controller($scope, $http, $uibModal) {

    $scope.const = {
      NAME_VOID: '',
      YES: 'Да',  
      NO: 'Нет'
    };

    $scope.arr_yes_no_void = [
      $scope.const.NAME_VOID,
      $scope.const.YES,
      $scope.const.NO
    ];

    $scope.form_data = {
        poster_list: [],
        film_list: [],
        film_details: null,
        filter_values: {
            year: $scope.const.NAME_VOID,
            country: $scope.const.NAME_VOID,
            genre: $scope.const.NAME_VOID,
            award: $scope.const.NAME_VOID,
            director: $scope.const.NAME_VOID,
            operator: $scope.const.NAME_VOID,
            producer: $scope.const.NAME_VOID,
            scenario: $scope.const.NAME_VOID,
            actor: $scope.const.NAME_VOID,
            nameFilm: $scope.const.NAME_VOID,
            findText: $scope.const.NAME_VOID,
            minRating: $scope.const.NAME_VOID,
            maxRating: $scope.const.NAME_VOID,
            minRuntime: $scope.const.NAME_VOID,
            maxRuntime: $scope.const.NAME_VOID,
            minSize: $scope.const.NAME_VOID,
            maxSize: $scope.const.NAME_VOID,
            resolution: $scope.const.NAME_VOID,
            notFound: false,
            status: $scope.const.NAME_VOID,
            isMovie: $scope.const.NAME_VOID,
            isOnLine: $scope.const.NAME_VOID
        },
        film_count: null,

        sort_fields: {
            title: 'Наименованию',
            year: 'Году',
            rating_ball: 'Рейтингу'
        },
        sortBy: 'title',

        sort_directions: {
            asc: 'по возрастанию',
            desc: 'по убыванию'
        },
        sortDirection: 'asc',
        is_admin: false,

        status_CONFIRMED: config['StatusMovieKeyNames']['CONFIRMED'],
        status_NOT_MATCH: config['StatusMovieKeyNames']['NOT_MATCH'],
        new_movies: 0
    };

    function get_person_list_by_position (data_list, position){
        var person_dict = {}, person_list;
        _.map(_.filter(_.flatten(_.values(data_list)), function(i){ return i.position == position; }), function(item){
            person_dict[item['person__id']] = { name: item['person__name'], id: item['person__id'] };
        });
        person_list = _.sortBy(_.values(person_dict), function(i){ return i.name });
        person_list.unshift({ name: $scope.const.NAME_VOID, id: '' });
        return person_list;
    }

    $scope.loadFilmList = function(){
        var params = {};
        $http.post(config['url']['get_film_list'], params).success(function(data){

          if (data['error']) {
              err_msg($scope, data['error']);
          }else {
              if (data['films']) {
                  $scope.form_data.film_list = _.sortBy(data['films'], 'title');
                  $scope.form_data.film_count = data['films'].length;
                  $scope.form_data.countries = data['countries'];
                  $scope.form_data.genres = data['genres'];
                  $scope.form_data.awards = data['awards'];
                  $scope.form_data.persons = data['persons'];
                  $scope.form_data.new_movies = data['new_movies'];

                  // массивы для фильтров
                  $scope.form_data.filter_years = _.sortBy(_.uniq(_.pluck(data['films'], 'year')));
                  $scope.form_data.filter_years.unshift($scope.const.NAME_VOID);
                  $scope.form_data.filter_countries = _.sortBy(_.uniq(_.flatten(_.values(data['countries']))));
                  $scope.form_data.filter_countries.unshift($scope.const.NAME_VOID);
                  $scope.form_data.filter_genres = _.sortBy(_.uniq(_.flatten(_.values(data['genres']))));
                  $scope.form_data.filter_genres.unshift($scope.const.NAME_VOID);
                  // $scope.form_data.filter_resolutions = _.sortBy(_.uniq(_.flatten(_.values(data['resolutions']))), function(item){ return item['w']; });
                  $scope.form_data.filter_resolutions = data['resolutions'];
                  $scope.form_data.filter_resolutions.unshift($scope.const.NAME_VOID);

                  $scope.form_data.filter_directors = get_person_list_by_position(data['persons'], config['PositionPerson']['DIRECTOR']);
                  $scope.form_data.filter_operators = get_person_list_by_position(data['persons'], config['PositionPerson']['OPERATOR']);
                  $scope.form_data.filter_producers = get_person_list_by_position(data['persons'], config['PositionPerson']['PRODUCER']);
                  $scope.form_data.filter_scenarios = get_person_list_by_position(data['persons'], config['PositionPerson']['SCENARIO']);
                  $scope.form_data.filter_actors = get_person_list_by_position(data['persons'], config['PositionPerson']['ACTOR']);

                  var award_dict = {};
                  _.map(_.flatten(_.values(data['awards'])), function(item){ var key = item.split(',')[0]; award_dict[key] = key; });
                  $scope.form_data.filter_awards = _.sortBy(_.values(award_dict));
                  $scope.form_data.filter_awards.unshift($scope.const.NAME_VOID);

                  $scope.form_data.filter_statuses = angular.copy(config['StatusMovieList']);
                  $scope.form_data.filter_statuses.unshift({ name: $scope.const.NAME_VOID, id: '' });
                  $scope.form_data.filter_statuses_dict = angular.copy(config['StatusMovieDict']);

                  // параметры
                  $scope.form_data.is_admin = data['is_admin'];
                  if (data['is_admin']){
                      $scope.form_data.filter_values.status = $scope.const.NAME_VOID;
                  }

                  // фильм в окне просмотра нужно определить заново, т.к. сам объект теперь другой
                  if ($scope.form_data.film_details != null){
                      var film = _.find($scope.form_data.film_list, function(item){ return item.id == $scope.form_data.film_details.id; });
                      $scope.film_edit(film || data['films'][0]);
                  }
              }
          }
        }).error(function(){
            err_msg($scope, "Ошибка при выполнении запроса.");
        });
    };
    $scope.loadFilmList();

    $scope.film_edit = function(film) {
        $scope.form_data.film_details = film;
        $scope.form_data.film_details.countries = $scope.form_data.countries[film.id];
        $scope.form_data.film_details.genres = $scope.form_data.genres[film.id];
        $scope.form_data.film_details.awards = $scope.form_data.awards[film.id];
        $scope.form_data.film_details.directors = $scope.form_data.persons[film.id + ", " + config['PositionPerson']['DIRECTOR']];
        $scope.form_data.film_details.operators = $scope.form_data.persons[film.id + ", " + config['PositionPerson']['OPERATOR']];
        $scope.form_data.film_details.producers = $scope.form_data.persons[film.id + ", " + config['PositionPerson']['PRODUCER']];
        $scope.form_data.film_details.scenarios = $scope.form_data.persons[film.id + ", " + config['PositionPerson']['SCENARIO']];
        $scope.form_data.film_details.actors = $scope.form_data.persons[film.id + ", " + config['PositionPerson']['ACTOR']];
    };

    function is_false_person_filter(film_id, position){
        var val_filter = parseInt($scope.form_data.filter_values[position]),
            person_ids = _.pluck($scope.form_data.persons[film_id + ", " + config['PositionPerson'][position.toUpperCase()]], 'person__id');
        return val_filter && person_ids.indexOf(val_filter) == -1;
    }

    function is_contains_award(item){
        return item.toLowerCase().indexOf($scope.form_data.filter_values.award.toLowerCase()) != -1;
    }

    function is_contains_string(item){
        return item.toLowerCase().indexOf($scope.form_data.filter_values.findText) != -1;
    }

    function is_contains_person(film_id, position){
        var person_names = _.pluck($scope.form_data.persons[film_id + ", " + config['PositionPerson'][position.toUpperCase()]], 'person__name');
        return _.some(person_names, is_contains_string);
    }

    // полный список параметров - function(value, index, array)
    $scope.setFilter = function(value) {

        // фильтры точного соответствия (нашли первое несоответствие - отбор не прошел)
        if ($scope.form_data.filter_values.year && value.year != $scope.form_data.filter_values.year){ return false; }
        if ($scope.form_data.filter_values.country && (!$scope.form_data.countries[value.id] || $scope.form_data.countries[value.id].indexOf($scope.form_data.filter_values.country) == -1)){ return false; }
        if ($scope.form_data.filter_values.genre && (!$scope.form_data.genres[value.id] || $scope.form_data.genres[value.id].indexOf($scope.form_data.filter_values.genre) == -1)){ return false; }
        if ($scope.form_data.filter_values.award){
            if (!_.some($scope.form_data.awards[value.id], is_contains_award)) { return false; }
        }
        if (is_false_person_filter(value.id, 'director')) { return false; }
        if (is_false_person_filter(value.id, 'operator')) { return false; }
        if (is_false_person_filter(value.id, 'producer')) { return false; }
        if (is_false_person_filter(value.id, 'scenario')) { return false; }
        if (is_false_person_filter(value.id, 'actor')) { return false; }
        if ($scope.form_data.filter_values.minRating){
            $scope.form_data.filter_values.minRating = $scope.form_data.filter_values.minRating.replace(/[,]/g, '.').replace(/[^0-9.]/g, '');
            if (value['rating_ball'] <= $scope.form_data.filter_values.minRating){ return false; }
        }
        if ($scope.form_data.filter_values.maxRating){
            $scope.form_data.filter_values.maxRating = $scope.form_data.filter_values.maxRating.replace(/[,]/g, '.').replace(/[^0-9.]/g, '');
            if (value['rating_ball'] >= $scope.form_data.filter_values.maxRating){ return false; }
        }
        if ($scope.form_data.filter_values.minRuntime){
            if (value['runtime'] <= $scope.form_data.filter_values.minRuntime){ return false; }
        }
        if ($scope.form_data.filter_values.maxRuntime){
            if (value['runtime'] >= $scope.form_data.filter_values.maxRuntime){ return false; }
        }
        if ($scope.form_data.filter_values.minSize){
            if (value['metadata']['size_mb'] <= $scope.form_data.filter_values.minSize){ return false; }
        }
        if ($scope.form_data.filter_values.maxSize){
            if (value['metadata']['size_mb'] >= $scope.form_data.filter_values.maxSize){ return false; }
        }
        if ($scope.form_data.filter_values.resolution){
            var resolution = JSON.parse($scope.form_data.filter_values.resolution);
            if (value['metadata']['width'] < resolution['w']){ return false; }
            if (value['metadata']['width'] == resolution['w'] && value['metadata']['height'] < resolution['h']){ return false; }
        }
        if ($scope.form_data.filter_values.notFound){
            // фильм не найден, если заголовок пустой
            if (value['title']){ return false; }
        }
        if ($scope.form_data.filter_values.nameFilm){
            if (value.title.toLowerCase().indexOf($scope.form_data.filter_values.nameFilm.toLowerCase()) == -1){ return false; }
        }
        if ($scope.form_data.filter_values.status && value.status != $scope.form_data.filter_values.status){ return false; }

        if ($scope.form_data.filter_values.isMovie == $scope.const.YES && !value['is_movie']) { return false; }
        if ($scope.form_data.filter_values.isMovie == $scope.const.NO && value['is_movie']) { return false; }

        if ($scope.form_data.filter_values.isOnLine == $scope.const.YES && !(value['is_movie'] && value['is_converted'])) { return false; }
        if ($scope.form_data.filter_values.isOnLine == $scope.const.NO && !(value['is_movie'] && !value['is_converted'])) { return false; }

        // фильтры на вхождение в любое из полей (нашли первое соответствие - отбор прошел)
        var text = $scope.form_data.filter_values.findText = $scope.form_data.filter_values.findText.toLowerCase();
        if (text){
            if (String(value.year).indexOf(text) != -1){ return true; }
            if (_.some($scope.form_data.countries[value.id], is_contains_string)) { return true; }
            if (is_contains_person(value.id, 'director')) { return true; }
            if (is_contains_person(value.id, 'operator')) { return true; }
            if (is_contains_person(value.id, 'producer')) { return true; }
            if (is_contains_person(value.id, 'scenario')) { return true; }
            if (is_contains_person(value.id, 'actor')) { return true; }
            if (value.title.toLowerCase().indexOf(text) != -1){ return true; }
            // если нигде не нашли, отбор не прошел
            return false;
        }

        return true;
    };

    $scope.setFilmDetail = function(film, param_key, param_val){
        var values = {};
        values[param_key] = param_val;

        var params = {
            film_id: film.id,
            values: values
        };
        $http.post(config['url']['set_params_film'], params)
            .success(function(data){
              if (data['error']) {
                  err_msg($scope, data['error']);
              }else {
                  if ($scope.form_data.film_details.hasOwnProperty(param_key)){
                      $scope.form_data.film_details[param_key] = data['data'][param_key];
                  }
              }
            })
            .error(function(){
              err_msg($scope, "Ошибка при выполнении запроса.");
            });

    };

    $scope.loadFilmKP = function(film){

        var params = {
            film_id: film.id
        };
        $http.post(config['url']['load_film_kp'], params)
            .success(function(data){
              if (data['error']) {
                  err_msg($scope, data['error']);
              }else {
                  $scope.loadFilmList();
              }
            })
            .error(function(){
              err_msg($scope, "Ошибка при выполнении запроса.");
            });

    };

    $scope.goExtRef = function(targetUrl){
        var otherWindow = window.open();
        otherWindow.opener = null;
        otherWindow.location = targetUrl;
    };

    $scope.deleteDetailsOK = function(args){
        var params = {
            film_id: args.add_params.film.id,
            delete_file: args.scope.params.delete_file
        };
        $http.post(config['url']['delete_film'], params)
            .success(function(data){
              if (data['error']) {
                  err_msg($scope, data['error']);
              }else {
                  $scope.loadFilmList();
              }
            })
            .error(function(){
              err_msg($scope, "Ошибка при выполнении запроса.");
            });

    };

    $scope.deleteDetails = function(film){
        var params = {film: film};
        confirm($uibModal, 'lg', true, $scope.deleteDetailsOK, 'partials/films/confirm_delete_detail.html', params);
    };
}



function confirm ($uibModal, size, isAnimationsEnabled, handlerOk, bodyTemplate, params) {
    // size - размер окна ("", "lg", "sm")
    // isAnimationsEnabled - анимация (true/false)

    if (typeof (isAnimationsEnabled) == 'undefined'){isAnimationsEnabled = true;}
    //this.bodyTags = bodyTags;

    var modalInstance = $uibModal.open({
      animation: isAnimationsEnabled,
      templateUrl: 'partials/common/confirm-modal.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
          title: function () { return "Удалить фильм из списка ?" },
          body: function () { return bodyTemplate; },
          fnPrepareArgsHandlerOk: function () { return function (scope){ return {scope: scope, add_params: params}; } }
      }
    });
    modalInstance.result.then(handlerOk, function (argsCancel) { });
  }


catalogApp.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, title, body, fnPrepareArgsHandlerOk) {

  $scope.params = {};
  $scope.params.title = title;
  $scope.params.body = body;
  $scope.params.delete_file = false;

  $scope.ok = function () {
    $uibModalInstance.close(fnPrepareArgsHandlerOk($scope));
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
