# coding=utf-8
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.http import JsonResponse

from models import UserRole, UserProfile

conf = settings.APP_CONFIG


def get_role_user(user):
    if not user or user == AnonymousUser():
        return None
    return user.userprofile.role


def is_admin(user):
    u""" Проверяет: пользователь админ? Если аутентификация отключена, то все пользователи админы
    """
    return get_role_user(user) == UserRole.ADMIN or settings.DISABLE_AUTH


def check_user_profile(f):
    u""" Декоратор проверяет наличие профиля пользователя. Если его нет, то создает его.
    """
    def wrapper(*args, **kwargs):

        user = args[0].user
        if user != AnonymousUser():
            # если пользователь не анонимный, проверим и создадим профиль
            try:
                user.userprofile
            except UserProfile.DoesNotExist:
                profile = UserProfile.objects.create(user=user)
                if user.is_superuser:
                    profile.role = UserRole.ADMIN
                    profile.save()

        return f(*args, **kwargs)

    return wrapper


def admin_role_required(f):
    u""" Декоратор проверяет что пользователь админ
    """
    def wrapper(*args, **kwargs):

        if conf.server.disable_auth or args[0].user.userprofile.role == UserRole.ADMIN:
            return f(*args, **kwargs)
        else:
            return JsonResponse({'error': 'admin required'})

    return wrapper
