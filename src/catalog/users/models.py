# coding=utf-8
from django.db import models
from django.contrib.auth.models import User


class UserRole(object):
    u""" Типы ролей пользователя
    """
    ADMIN = 1
    USER = 2

    names = {
        ADMIN: u'Администратор',
        USER: u'Пользователь',
    }


class UserProfile(models.Model):
    u""" Профиль пользователя
    """
    user = models.OneToOneField(
        User,
        verbose_name=u"Пользователь django",
        on_delete=models.CASCADE,
        db_index=True,
    )

    # длины полей джанговской модели django.contrib.auth.models.User недостаточны для хранения "русских фамилий"
    f_name = models.CharField(
        max_length=100,
        db_index=True,
        blank=True, null=True
    )
    i_name = models.CharField(
        max_length=100,
        db_index=True,
        blank=True, null=True
    )
    o_name = models.CharField(
        max_length=100,
        blank=True, null=True
    )

    role = models.PositiveSmallIntegerField(
        verbose_name=u"Роль пользователя в системе",
        choices=UserRole.names.items(),
        default=UserRole.USER
    )
