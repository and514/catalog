# coding=utf-8
import os

from django.conf import settings

from src.catalog.entity.models import DbMovie

conf = settings.APP_CONFIG


def get_movies_files_name_from_dir(directory, is_file_added=False, is_converted_only=True):
    u""" выдает список файлов фильмов в папке включая вложенные папки """
    result = []

    for top, dirs, files in os.walk(directory):
        for f in files:

            # берем только конвертированные фильмы
            if is_converted_only and f.split('.')[-1] != settings.MOVIE_CONVERTED_EXT:
                continue

            # берем только фильмы
            if f.split('.')[-1] not in conf.movies.input_ext:
                continue

            # если отключено обновление, пропускаем уже закачанные
            path_file = os.path.join(top, f)
            if not is_file_added and DbMovie.is_file_added(path_file):
                continue

            result.append({
                'path_file': path_file.encode('utf-8') if isinstance(path_file, unicode) else path_file,
                'name_file': f.encode('utf-8') if isinstance(f, unicode) else f
            })

    return result


def get_new_files(is_file_added=False):
    u""" возвращает список фильмов из папок в конфиге """

    # кодированные
    files = get_movies_files_name_from_dir(settings.DIR_MOVIES_OUTPUT, is_file_added=is_file_added)

    # некодированные
    map(lambda x: files.extend(x), [
        get_movies_files_name_from_dir(d, is_file_added=is_file_added, is_converted_only=False)
        for d in settings.DIR_MOVIES_INPUT_NOT_DECODED if os.path.exists(d)
        ])

    return files
