# coding=utf-8
from bs4 import BeautifulSoup
import requests


def get_request(url, params=None):
    return requests.get(url, params=params, headers={
        'User-Agent': 'Mozilla/5.0 (X11; U; Linux i686; ru; rv:1.9.1.8) Gecko/20100214 Linux Mint/8 (Helena) Firefox/3.5.8',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'ru,en-us;q=0.7,en;q=0.3',
        'Accept-Encoding': 'deflate',
        'Accept-Charset': 'windows-1251,utf-8;q=0.7,*;q=0.7',
        'Keep-Alive': '300',
        'Connection': 'keep-alive',
        'Referer': 'http://www.kinopoisk.ru/',
        'Cookie': 'users_info[check_sh_bool]=none; search_last_date=2010-02-19; search_last_month=2010-02;                                        PHPSESSID=b6df76a958983da150476d9cfa0aab18',
    })


def get_content(url, params):
    response = get_request(url, params=params)
    response.connection.close()
    return response.content.decode('windows-1251', 'ignore')


def get_rating_film(film_id):
    u""" свежий рейтинг
        возвращает два кортежа (ретинг, число голосовавших)
    """
    url = "http://rating.kinopoisk.ru/%d.xml" % film_id
    content = get_content(url, params={})
    rating_content = (content[content.find('<rating>'): content.find('</rating>')])
    content_soup = BeautifulSoup(rating_content, 'lxml')

    try:
        kp_rating = content_soup.find('kp_rating')
        result_kp = kp_rating.text, kp_rating.get('num_vote')
        imdb_rating = content_soup.find('imdb_rating')
        result_imdb = imdb_rating.text, imdb_rating.get('num_vote')
    except Exception:
        result_kp = "0", "0"
        result_imdb = "0", "0"

    return result_kp, result_imdb
