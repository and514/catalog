# coding:utf-8
from lib_packages_core.helpers.exeptions import AppException


class AppKpSearchException(AppException):
    def __repr__(self):
        return '{}: {}'.format(u'Не найдено соответствие на кинопоиске', self.message)