# coding=utf-8
import os

from django.conf import settings
from hachoir_metadata import extractMetadata
from hachoir_parser import createParser

from lib_packages_core.helpers.dates import seconds2strtime
from models import DbMovie


def get_movie_metadata(path):
    u""" достает данные о фильме из файла """
    def get_metadata_value(meta_, attr, default=0):
        try:
            return getattr(meta_, '_Metadata__data', {}) and meta_.get(attr, default) or default
        except Exception as e:
            return default

    result = {}
    try:
        meta = extractMetadata(createParser(path))
        size = round(os.path.getsize(path) / 1000000., 1)
    except Exception as e:
        meta = {}
        size = 0

    result['size'] = "%sMb" % size
    result['size_mb'] = size
    result['height'] = get_metadata_value(meta, 'height')
    result['width'] = get_metadata_value(meta, 'width')
    result['duration_str'] = seconds2strtime(getattr(get_metadata_value(meta, 'duration'), 'seconds', 0))
    return result


def get_poster_list_for_header(max_width, max_height):
    u""" Список постеров для шапки сайта """
    poster_list = []
    real_width = 0

    film_list = list(DbMovie.objects.all().order_by('?').values('image', 'image_height', 'image_width'))

    for film in film_list:
        poster_list.append(film['image'].replace(
            settings.BASE_DIR, "".join([settings.CURRENT_SITE_SCHEME, settings.CURRENT_SITE_HOST])).encode('utf-8'))
        k_img = float(max_height) / film['image_height']
        real_width += int(film['image_width'] * k_img)
        if real_width > max_width:
            break

    return poster_list
