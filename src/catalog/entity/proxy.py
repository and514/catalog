# coding=utf-8
from bs4 import BeautifulSoup
import os
import re
from subprocess import Popen
import urllib
from datetime import date

from django.conf import settings
from django.core.files.uploadedfile import TemporaryUploadedFile
from django.db import transaction

from lib_packages_core.helpers.numbers import str2number
from lib_packages_core.helpers.const import Month

from src.catalog.helpers.request import get_content
from src.catalog.entity.models import (
    DbMovie, uploader_poster, PositionPerson, DbPerson, DbMoviePersonMap, DbCountry, DbMovieCountryMap, DbGenre,
    DbMovieGenreMap, DbMovieAwardMap, DbAward)
from src.catalog.helpers.app_exception import AppKpSearchException

conf = settings.APP_CONFIG


class ParserMap(object):

    DATA_BASE = 1
    ADDITION = 2

    table_map = {
        u'год': 'year',
        u'слоган': 'tag_line',
        u'бюджет': 'budget',
        u'сборы в США': 'profit_usa',
        u'сборы в мире': 'profit_world',
        u'сборы в России': 'profit_russia',
        u'премьера (мир)': 'premier_world',
        u'возраст': 'age_limit',
        u'время': 'runtime',

        u'жанр': 'genres',
        u'страна': 'countries',
        u'режиссер': 'directors',
        u'сценарий': 'scenarios',
        u'продюсер': 'producers',
        u'оператор': 'operators',
    }

    actors_tag = 'actors'
    awards_tag = 'awards'


class ProxyMovie(object):
    u""" Фильмы """
    # урл для поиска id фильма
    url_search_id_film = '{}/index.php'.format(conf.kp.host)
    url_kinopoisk = '/film/%s/'
    url_image_template = '{}/images/film_big/%s.jpg'.format(conf.kp.host)
    url_awards_template = '{}/film/%s/awards'.format(conf.kp.host)

    exclude_values = [u'слова', u'сборы', '...']

    def __init__(self, *args, **kwargs):
        self.name_file = None
        self.obj = None
        self.additions = {}
        self.define_movie_obj(*args, **kwargs)

    def define_movie_obj(self, *args, **kwargs):
        path_file = kwargs.get('path_file', '')
        name_file = kwargs.get('name_file', '')

        self.name_file = name_file
        try:
            self.obj = DbMovie.objects.get(path_file=path_file)
        except DbMovie.DoesNotExist:
            self.obj = DbMovie(path_file=path_file)
            self.get_movie_data()

    def get_movie_data(self):
        movie_id = re.findall('[#]([0-9]+)[.]', self.name_file)
        if movie_id:
            self.obj.id_kinopoisk = movie_id[0]
        else:
            self.obj.id_kinopoisk = self.get_id_kinopoisk()

        # основной контент
        self.get_main_content()
        # актеры
        self.get_awards()

    def trans_name_file(self, name_file):
        trans_name = ' '.join(self.name_file.split('.')[:-1])
        # (12200fsd3)
        trans_name = re.sub('\([\w]+\)', '', trans_name)
        # [12200dsaf3]
        trans_name = re.sub('\[[\w]+\]', '', trans_name)
        # [_] на пробелы
        trans_name = re.sub('[_]+', ' ', trans_name)

        return trans_name

    def get_id_kinopoisk(self):
        u""" находит id фильма """

        trans_name = self.trans_name_file(self.name_file)

        params = {
            'level': 7,
            'from': 'forma',
            'result': 'adv',
            'm_act[from]': 'forma',
            'm_act[what]': 'content',
            'm_act[find]': trans_name
        }
        content = get_content(self.url_search_id_film, params=params)
        try:
            content_start = content[content.find('<div class="element most_wanted">'):]
            content_start = content_start[:content_start.find('<div class="search_results">')]
            content_result = content_start[content_start.find('<div class="info">'):]
            results_soup = BeautifulSoup(content_result, 'lxml')
            results = results_soup.find('p', {'class': 'name'})
            tag_a = results.find('a')
            film_id = re.findall('/film/([0-9]+)/', tag_a.get('href'))[0]
        except (ValueError, IndexError, AttributeError) as e:
            raise AppKpSearchException(e.message)
        else:
            return film_id

    def get_main_content(self):
        assert self.obj.id_kinopoisk

        self.obj.url_kinopoisk = self.url_kinopoisk % self.obj.id_kinopoisk
        content = get_content(u'%s%s' % (conf.kp.host, self.obj.url_kinopoisk), params={})

        # заголовок
        title_content = (
            content[
                content.find('<div id="headerFilm" class="feature_film_background'):
                content.find('<div id="photoBlock" class="originalPoster">')])
        title_soup = BeautifulSoup(title_content, 'lxml')
        title = title_soup.find('h1', {'class': 'moviename-big', 'itemprop': 'name'})
        if title:
            tag_a = title.find('a')
            if tag_a:
                self.obj.episodes = tag_a.get('href')
            self.obj.title = title.text
        title = title_soup.find('span', {'itemprop': 'alternativeHeadline'})
        if title:
            self.obj.title_original = title.text

        # постер
        poster_file = "%s.jpg" % self.obj.id_kinopoisk
        resp = urllib.urlopen(self.url_image_template % self.obj.id_kinopoisk).read()
        tf = TemporaryUploadedFile(poster_file, 'image/jpeg', len(resp), 'binary')
        tf.file.write(resp)
        self.obj.image = tf

        # рейтинг
        rating_content = (
            content[
                content.find('<a href="/film/%s/votes/" class="continue rating_link' % self.obj.id_kinopoisk):
                content.find('<div style="color:#999;font:100 11px tahoma, verdana">')])
        rating_soup = BeautifulSoup(rating_content, 'lxml')
        rating = rating_soup.find('span', {'class': 'rating_ball'})
        if rating:
            self.obj.rating_ball = rating.text
        rating = rating_soup.find('span', {'class': 'ratingCount', 'itemprop': 'ratingCount'})
        if rating:
            self.obj.rating_count = rating.text

        # сюжет
        plot_content = (
            content[
                content.find('<tr><td colspan=3 style="padding: 10px 10px 15px 20px" class="news">'):
                content.find('<form class="rating_stars" name="voting_film_form" action="" method="get">')])
        plot_soup = BeautifulSoup(plot_content, 'lxml')
        plot = plot_soup.find('div', {'class': 'brand_words', 'itemprop': 'description'})
        if plot:
            self.obj.plot = plot.text

        # основной контент
        main_content = (
            content[
                content.find('<div id="infoTable">'):
                content.find('<div id="actorList" class="clearfix actorListbg">')])
        main_soup = BeautifulSoup(main_content, 'lxml')

        table_info = main_soup.find('table', {'class': 'info'})
        if table_info:
            for tr in table_info.findAll('tr'):
                tds = tr.findAll('td')
                name = tds[0].text
                value = tds[1]

                if name not in ParserMap.table_map:
                    continue

                a_tags = value.findAll('a')
                if not a_tags:
                    if hasattr(self.obj, ParserMap.table_map[name]):
                        setattr(self.obj, ParserMap.table_map[name], value.text)
                    continue

                if name == u'год':
                    a_tags = a_tags[:1]

                for a in a_tags:
                    if a.text in self.exclude_values:
                        continue
                    if hasattr(self.obj, ParserMap.table_map[name]):
                        setattr(self.obj, ParserMap.table_map[name], a.text)
                    else:
                        if ParserMap.table_map[name] not in self.additions:
                            self.additions[ParserMap.table_map[name]] = []
                        self.additions[ParserMap.table_map[name]].append((a.get('href'), a.text))

        # актеры
        actors_content = (
            content[
                content.find('<ul><li itemprop="actors">'):
                content.find(u'<h4><a href="/film/640980/cast/">показать всех</a> &raquo;</h4>')])
        actors_soup = BeautifulSoup(actors_content, 'lxml')
        ul_soup = actors_soup.findAll('ul')
        if ul_soup:
            ul_soup = ul_soup[0]
            for a in ul_soup.findAll('a'):
                if a.text in self.exclude_values:
                    continue
                if ParserMap.actors_tag not in self.additions:
                    self.additions[ParserMap.actors_tag] = []
                self.additions[ParserMap.actors_tag].append((a.get('href'), a.text))

    def get_awards(self):
        u""" награды """
        assert self.obj.id_kinopoisk

        content = get_content(self.url_awards_template % self.obj.id_kinopoisk, params={})
        awards_content = (
            content[
                content.find('<table cellspacing="0" cellpadding="0" border="0" width="100%" style="background'):
                content.find('<table cellspacing=0 cellpadding=4 width=100% border=0 style="text-align: left">')])
        awards_soup = BeautifulSoup(awards_content, 'lxml')

        if awards_soup:

            if ParserMap.awards_tag not in self.additions:
                self.additions[ParserMap.awards_tag] = []
            awards = self.additions[ParserMap.awards_tag]

            # по каждой группе наград
            for t in awards_soup.findAll('table'):
                group_dict = {}
                # type_award = None
                tds = t.findAll('td', {'class': 'news'})
                for i, td in enumerate(tds):
                    # заголовок
                    if i == 0:
                        group_dict['title'] = (td.find('a').get('href'), td.text)
                        group_dict['detail'] = []
                        continue

                    lis = td.findAll('li', {'class': 'trivia'})
                    if not lis and re.sub(r'[\s\xa0]', '', td.text):
                        # тип награды (победитель или номинация)
                        group_dict['detail'].append(re.sub(r'[:\xa0]', '', td.text))
                    # TODO детали пока убрал
                    # else:
                    #     for li in lis:
                    #         if type_award not in group_dict:
                    #             group_dict[type_award] = []
                    #         group_dict[type_award].append((li.find('a').get('href'), li.text))

                awards.append(group_dict)

    @transaction.atomic
    def save(self):
        u""" Сохранение """
        def re_profit_world(str_dollar):
            if isinstance(str_dollar, basestring) and str_dollar:
                str_dollar = str_dollar.split('=')[-1]
            return str2number(str_dollar)

        def re_runtime(str_time):
            if isinstance(str_time, basestring) and str_time:
                str_time = str_time.split(' ')[0]
            return str2number(str_time)

        # постер (если файл есть, удалим его)
        poster_file = "%s.jpg" % self.obj.id_kinopoisk
        path_poster_file = uploader_poster(self.obj, poster_file)
        if os.path.isfile(path_poster_file):
            Popen(["rm", path_poster_file])

        self.obj.budget = str2number(self.obj.budget)
        self.obj.profit_russia = str2number(self.obj.profit_russia)
        self.obj.profit_usa = str2number(self.obj.profit_usa)
        self.obj.profit_world = re_profit_world(self.obj.profit_world)
        self.obj.rating_ball = float(self.obj.rating_ball) if self.obj.rating_ball else None
        self.obj.rating_count = str2number(self.obj.rating_count)
        self.obj.runtime = re_runtime(self.obj.runtime)
        # мировая премьера
        if self.additions.get('premier_world', []):
            self.obj.url_premier = self.additions['premier_world'][0][0]
            d, m, y = self.additions['premier_world'][0][1].split()
            self.obj.date_premier = date(int(y), Month.names_at[m], int(d))
        self.obj.save()

        # люди
        self.save_person(self.additions.get('actors', []), PositionPerson.ACTOR)
        self.save_person(self.additions.get('directors', []), PositionPerson.DIRECTOR)
        self.save_person(self.additions.get('operators', []), PositionPerson.OPERATOR)
        self.save_person(self.additions.get('producers', []), PositionPerson.PRODUCER)
        self.save_person(self.additions.get('scenarios', []), PositionPerson.SCENARIO)
        # страны
        self.save_country(self.additions.get('countries', []))
        # жанры
        self.save_genre(self.additions.get('genres', []))
        # награды
        self.save_award(self.additions.get('awards', []))

    def save_person(self, persons, position):
        u""" Сохраняет актеров, режиссеров и т.д. """
        for url_kinopoisk, name in persons:
            person, _ = DbPerson.objects.get_or_create(url_kinopoisk=url_kinopoisk, name=name)
            _ = DbMoviePersonMap.objects.get_or_create(movie=self.obj, person=person, position=position)

    def save_country(self, arr):
        u""" Сохраняет страны и привязывает их к DbMovie """
        for url_kinopoisk, name in arr:
            country, _ = DbCountry.objects.get_or_create(url_kinopoisk=url_kinopoisk, name=name)
            _, _ = DbMovieCountryMap.objects.get_or_create(movie=self.obj, country=country)

    def save_genre(self, arr):
        u""" Сохраняет жанры """
        for url_kinopoisk, name in arr:
            genre, _ = DbGenre.objects.get_or_create(url_kinopoisk=url_kinopoisk, name=name)
            _, _ = DbMovieGenreMap.objects.get_or_create(movie=self.obj, genre=genre)

    def save_award(self, arr):
        u""" Сохраняет награды """
        for award_dict in arr:
            url_kinopoisk, name = award_dict['title']
            details = ", ".join(award_dict['detail'])
            award, _ = DbAward.objects.get_or_create(url_kinopoisk=url_kinopoisk, name=name)
            _, _ = DbMovieAwardMap.objects.get_or_create(movie=self.obj, award=award, details=details)


class ProxyMovieReload(ProxyMovie):
    u""" Прокси для повторной загрузки по заданному id_kinopoisk """

    def define_movie_obj(self, *args, **kwargs):
        movie = kwargs.get('movie')

        if isinstance(movie, int):
            self.obj = DbMovie.objects.get(id=movie)
        else:
            self.obj = movie
        self.get_movie_data()

    def get_movie_data(self):
        assert self.obj.id_kinopoisk
        # основной контент
        self.get_main_content()
        # актеры
        self.get_awards()
