# coding=utf-8
from os import path

from django.conf import settings
from django.db import models


class PositionPerson(object):
    ACTOR = 1
    DIRECTOR = 2
    OPERATOR = 3
    PRODUCER = 4
    SCENARIO = 5

    names = {
        ACTOR: u'Актер',
        DIRECTOR: u'Режиссер',
        OPERATOR: u'Оператор',
        PRODUCER: u'Продюсер',
        SCENARIO: u'Сценарий'
    }

    key_names = {
        'ACTOR': ACTOR,
        'DIRECTOR': DIRECTOR,
        'OPERATOR': OPERATOR,
        'PRODUCER': PRODUCER,
        'SCENARIO': SCENARIO
    }


class StatusMovie(object):
    NEW = 1
    CONFIRMED = 2
    NOT_MATCH = 3

    names = {
        NEW: u'Новый',
        CONFIRMED: u'Подтвержденный',
        NOT_MATCH: u'Не соответствует',
    }

    key_names = {
        'NEW': NEW,
        'CONFIRMED': CONFIRMED,
        'NOT_MATCH': NOT_MATCH,
    }


# noinspection PyUnusedLocal
def uploader_poster(instance, filename):
    # u""" полный путь к постеру на диске """
    return path.join(settings.UPLOAD_ROOT, settings.BIG_IMAGES_DIR, filename)


class BaseModel(models.Model):
    u""" абстрактная базовая модель """
    class Meta:
        abstract = True


class DbMovie(BaseModel):
    u""" Фильмы """
    # обязательные поля
    path_file = models.CharField(
        verbose_name=u"Путь к файлу на диске",
        max_length=255,
    )

    # необязательные
    discrepancy = models.BooleanField(
        verbose_name=u"Несоответствие фильма и данных кинопоиска",
        default=False,
    )
    id_kinopoisk = models.PositiveIntegerField(
        verbose_name=u"ID кинопоиска",
        blank=True, null=True,
    )
    title = models.CharField(
        verbose_name=u"Заголовок",
        max_length=200,
        blank=True, null=True,
    )
    title_original = models.CharField(
        verbose_name=u"Оригинальный заголовок",
        max_length=200,
        blank=True, null=True,
    )
    url_kinopoisk = models.URLField(
        verbose_name=u"URL в кинопоиске",
        blank=True, null=True,
    )
    plot = models.TextField(
        verbose_name=u"Сюжет",
        blank=True, null=True,
    )
    year = models.PositiveSmallIntegerField(
        verbose_name=u"Год",
        blank=True, null=True,
    )
    tag_line = models.CharField(
        verbose_name=u"Слоган",
        max_length=200,
        blank=True, null=True,
    )
    runtime = models.PositiveSmallIntegerField(
        verbose_name=u"Продолжительность",
        blank=True, null=True,
    )
    url_episodes = models.URLField(
        verbose_name=u"Сериал (эпизоды)",
        blank=True, null=True,
    )

    budget = models.PositiveIntegerField(
        verbose_name=u"Бюджет",
        blank=True, null=True,
    )
    profit_usa = models.PositiveIntegerField(
        verbose_name=u"Сборы в США",
        blank=True, null=True,
    )
    profit_russia = models.PositiveIntegerField(
        verbose_name=u"Сборы в России",
        blank=True, null=True,
    )
    profit_world = models.PositiveIntegerField(
        verbose_name=u"Сборы в мире",
        blank=True, null=True,
    )

    rating_ball = models.FloatField(
        verbose_name=u"Рейтинг Кинопоиска (средний балл)",
        blank=True, null=True,
    )
    rating_count = models.PositiveIntegerField(
        verbose_name=u"Рейтинг Кинопоиска (количество)",
        blank=True, null=True,
    )

    url_premier = models.URLField(
        verbose_name=u"Мировая премьера (ссылка)",
        blank=True, null=True,
    )
    date_premier = models.DateField(
        verbose_name=u'Мировая премьера (дата)',
        blank=True, null=True,
    )

    image = models.ImageField(
        upload_to=uploader_poster,
        verbose_name=u"Постер",
        max_length=255,
        blank=True, null=True,
        height_field='image_height',
        width_field='image_width',
    )
    image_width = models.PositiveIntegerField(
        verbose_name=u"Ширина изображения",
        editable=False,
        null=True, blank=True,
    )
    image_height = models.PositiveIntegerField(
        verbose_name=u"Высота изображения",
        editable=False,
        null=True, blank=True,
    )

    status = models.PositiveSmallIntegerField(
        verbose_name=u"Статус",
        choices=StatusMovie.names.items(),
        default=StatusMovie.NEW
    )

    @classmethod
    def is_file_added(cls, name_file):
        return DbMovie.objects.filter(path_file=name_file).exists()

    class Meta:
        db_table = 'movie'
        unique_together = ('path_file',)


class DbCountry(BaseModel):
    u""" Страны """
    url_kinopoisk = models.URLField(
        verbose_name=u"URL в кинопоиске",
        blank=True, null=True,
    )
    name = models.CharField(
        verbose_name=u"Наименование",
        max_length=100
    )

    class Meta:
        db_table = 'country'
        unique_together = ('url_kinopoisk',)


class DbMovieCountryMap(BaseModel):
    u""" Связка - фильмы, страны """
    movie = models.ForeignKey(
        DbMovie,
        verbose_name=u"Фильм",
    )
    country = models.ForeignKey(
        DbCountry,
        verbose_name=u"Страна",
    )

    class Meta:
        db_table = 'map_movie_country'


class DbPerson(BaseModel):
    u""" Люди """
    url_kinopoisk = models.URLField(
        verbose_name=u"URL в кинопоиске",
        blank=True, null=True,
    )
    name = models.CharField(
        verbose_name=u"Имя Фамилия",
        max_length=100,
    )
    name_original = models.CharField(
        verbose_name=u"Оригинальноые Имя Фамилия",
        max_length=100,
        blank=True, null=True,
    )
    year_birth = models.PositiveSmallIntegerField(
        verbose_name=u"Год рождения",
        blank=True, null=True,
    )
    information = models.TextField(
        verbose_name=u"Информация",
        blank=True, null=True,
    )

    class Meta:
        db_table = 'person'
        unique_together = ('url_kinopoisk',)


class DbMoviePersonMap(BaseModel):
    u""" Связка - фильмы, (актеры, режиссеры, ...) """
    movie = models.ForeignKey(
        DbMovie,
        verbose_name=u"Фильм",
    )
    person = models.ForeignKey(
        DbPerson,
        verbose_name=u"Актеры",
    )
    position = models.PositiveSmallIntegerField(
        verbose_name=u"Вид участия",
        blank=True, null=True,
        choices=PositionPerson.names.items()
    )

    class Meta:
        db_table = 'map_movie_person'


class DbGenre(BaseModel):
    u""" Жанры """
    url_kinopoisk = models.URLField(
        verbose_name=u"URL в кинопоиске",
        blank=True, null=True,
    )
    name = models.CharField(
        verbose_name=u"Наименование",
        max_length=100
    )

    class Meta:
        db_table = 'genre'
        unique_together = ('url_kinopoisk',)


class DbMovieGenreMap(BaseModel):
    u""" Связка - фильмы, жанры """
    movie = models.ForeignKey(
        DbMovie,
        verbose_name=u"Фильм",
    )
    genre = models.ForeignKey(
        DbGenre,
        verbose_name=u"Жанр",
    )

    class Meta:
        db_table = 'map_movie_genre'


class DbAward(BaseModel):
    u""" Награды """
    url_kinopoisk = models.URLField(
        verbose_name=u"URL в кинопоиске",
        blank=True, null=True,
    )
    name = models.CharField(
        verbose_name=u"Наименование",
        max_length=100
    )

    class Meta:
        db_table = 'award'
        unique_together = ('url_kinopoisk',)


class DbMovieAwardMap(BaseModel):
    u""" Связка - фильмы, награды """
    movie = models.ForeignKey(
        DbMovie,
        verbose_name=u"Фильм",
    )
    award = models.ForeignKey(
        DbAward,
        verbose_name=u"Награда",
    )
    details = models.TextField(
        verbose_name=u'Количество наград'
    )

    class Meta:
        db_table = 'map_movie_award'
