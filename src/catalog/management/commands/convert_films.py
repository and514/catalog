# coding=utf-8
from optparse import make_option
import os
from subprocess import Popen
import sys

from django.conf import settings
from django.core.management.base import BaseCommand

from lib_packages_core.helpers.commands import run_is_not_running
from lib_packages_core.helpers.dates import datetime2str, datetime_now
from lib_packages_core.helpers.files import remove_dir_rec, create_path_dir
from lib_packages_core.helpers.log import log_error, log_info


conf = settings.APP_CONFIG
LOGGER = settings.LOGGER_FILE_CONVERT_FILMS


class Command(BaseCommand):
    u"""
        Находит фильмы в input папке, конвертирует в tmp папке и складывет в out папку.
        При повторной загрузке, конвертированный ранее заменяется.
        Оригинальный файл после конвертации удаляется.

        Опции:
            --limit, -l количество файлов для обработки за один сеанс скрипта
    """
    help = 'finds movies in directory and convert'
    
    option_list = BaseCommand.option_list + (
        make_option('--limit', "-l", action='store', help='limit convert files', default=sys.maxsize),
    )

    @run_is_not_running
    def handle(self, *args, **options):

        limit = options.get('limit')
        dir_tmp = settings.DIR_MOVIES_TMP
        dir_input = settings.DIR_MOVIES_INPUT
        dir_output = settings.DIR_MOVIES_OUTPUT

        log_info(u'%s Начинаем конвертацию' % datetime2str(datetime_now()))
        count = 0
        count_move = 0
        count_convert = 0

        # чистим временную папку
        if os.path.exists(dir_tmp):
            remove_dir_rec(dir_tmp)
        create_path_dir(dir_tmp)

        for top, dirs, files in os.walk(dir_input):
            for f in files:

                # берем только фильмы
                if f.split('.')[-1] not in conf.movies.input_ext:
                    continue

                out_file = ".".join([" ".join(f.split('.')[:-1]), settings.MOVIE_CONVERTED_EXT])

                path_input = os.path.join(top, f).decode('utf-8')
                path_output = os.path.join(dir_output, out_file).decode('utf-8')
                path_tmp = os.path.join(dir_tmp, out_file).decode('utf-8')

                try:

                    # удаляем загруженный ранее
                    if os.path.isfile(path_output):
                        os.remove(path_output)

                    if f == out_file:
                        # файл уже в нужном формате
                        operation_name = u"Перемещение"
                        Popen(["mv", path_input, path_output])
                        count_move += 1
                    else:
                        operation_name = u"Конвертация"
                        p = Popen(["ffmpeg", "-i", path_input, "-vcodec", "h264", "-acodec", "ac3_fixed", path_tmp])
                        msg = p.communicate()[1]
                        if msg:
                            raise Exception(msg)
                        Popen(["mv", path_tmp, path_output])
                        # удалим оригинал
                        os.remove(path_input)
                        count_convert += 1

                except Exception, e:
                    log_error(u"%s Ошибка (%s): %s" % (
                        datetime2str(datetime_now()), path_input, e.message), logger=LOGGER)
                else:
                    count += 1
                    msg = u"%s %s: %s из %s в %s" % (
                        datetime2str(datetime_now()), operation_name, count, path_input, path_output)
                    log_info(msg.encode('utf-8'), logger=LOGGER)

                if count >= limit:
                    break

        log_info(u"%s Обработка закончена. Конвертировано: %d. Перемещено: %d" %
                 (datetime2str(datetime_now()), count_convert, count_move),
                 logger=LOGGER)
