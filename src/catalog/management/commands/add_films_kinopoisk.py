# coding=utf-8
from optparse import make_option

from django.conf import settings
from django.core.management.base import BaseCommand

from lib_packages_core.helpers.commands import run_is_not_running
from lib_packages_core.helpers.dates import datetime_now, datetime2str
from lib_packages_core.helpers.log import log_error, log_info

from src.catalog.entity.proxy import ProxyMovie
from src.catalog.entity.models import StatusMovie
from src.catalog.helpers.files import get_new_files

LOGGER = settings.LOGGER_FILE_ADD_FILMS


class Command(BaseCommand):
    u"""
        Находит фильмы на кинопоиске и добавляет в БД
        Опции:
            --path, -p путь к папке с фильмами
    """
    help = 'finds movies on kinopoisk and added to the database'
    
    option_list = BaseCommand.option_list + (
        make_option('--reload', "-r", action='store', help='Reload exists data', default=False),
    )

    @run_is_not_running
    def handle(self, *args, **options):

        # признак обновления данных
        is_file_added = options.get('reload') == 'true'

        log_info(u'%s Начинаем добавление в каталог' % datetime2str(datetime_now()))
        count = 0

        files = get_new_files(is_file_added)

        for f in files:

            u_name_file = f['name_file'] if isinstance(f['name_file'], unicode) else f['name_file'].decode('utf-8')
            obj = None
            try:
                obj = ProxyMovie(path_file=f['path_file'], name_file=f['name_file'])
                obj.save()
            except Exception, e:
                if obj is not None:
                    obj.status = StatusMovie.NOT_MATCH
                    obj.save()
                log_error(u"%s Ошибка (%s): %s" %
                          (datetime2str(datetime_now()), u_name_file, e.message), logger=LOGGER)
            else:
                count += 1
                log_info(u"%s Добавлен: %d - %s, %s" %
                         (datetime2str(datetime_now()), count, obj.obj.title, u_name_file),
                         logger=LOGGER)

        log_info(u"%s Обработка закончена" % datetime2str(datetime_now()))
