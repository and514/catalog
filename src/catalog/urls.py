"""untitled URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

import src.catalog.views as catalog_views
from my_packages.registration.const import HOME_URL_NAME_SPACE, REG_PACKAGE_URL


urlpatterns = [

    url(r'^%s/' % REG_PACKAGE_URL, include('my_packages.registration.urls')),
    url(r'^$', catalog_views.main_view, name=HOME_URL_NAME_SPACE),

    url(r'^admin/', admin.site.urls),
    url(r'^conf/config.js$', catalog_views.config_js),
    url(r'^film/list$', catalog_views.get_film_list),
    url(r'^film/set_params_film$', catalog_views.set_params_film),
    url(r'^film/load_film_kp$', catalog_views.load_film_kp),
    url(r'^film/delete_film$', catalog_views.delete_film),
]
