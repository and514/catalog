# coding=utf-8
import os
from collections import defaultdict

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import render_to_response
from django.views.decorators.csrf import requires_csrf_token
from django.middleware import csrf
from django.template.context_processors import csrf as context_csrf
from subprocess import Popen

from lib_packages_core.helpers.dates import date2str
from src.catalog.entity.helpers import get_movie_metadata, get_poster_list_for_header
from src.catalog.entity.models import (
    DbMovie, DbMovieCountryMap, DbMoviePersonMap, PositionPerson, DbMovieGenreMap, DbMovieAwardMap, StatusMovie)
from src.catalog.entity.proxy import ProxyMovieReload
from src.catalog.helpers.files import get_new_files
from src.catalog.users.helpers import is_admin, check_user_profile, admin_role_required

conf = settings.APP_CONFIG


@check_user_profile
def main_view(request):
    params = {
        # пользователь авторизован или авторизация не требуется
        'user_auth': request.user.is_authenticated() or settings.DISABLE_AUTH,
        'user_name': request.user.username,
        # пользователь админ (авторизован) или авторизация не требуется
        'is_admin': is_admin(request.user),
        'PosterListForHeader': get_poster_list_for_header(2500, 150),
        'disable_auth': 'true' if settings.DISABLE_AUTH else 'false',
    }
    csrf.get_token(request)
    params.update(context_csrf(request))
    return render_to_response('index.html', params)


@requires_csrf_token
def get_film_list(request):
    u""" Список фильмов """
    def get_values_list(model_map, rel_field, _filter=None):
        if _filter is None:
            _filter = {}
        obj_list = model_map.objects.filter(**_filter).values_list('movie_id', '%s__name' % rel_field)
        obj_dict = defaultdict(list)
        map(lambda x_: obj_dict[x_[0]].append(x_[1]), obj_list)
        return obj_dict

    def get_person_list():
        obj_list = DbMoviePersonMap.objects.values('movie_id', 'position', 'person__id', 'person__name')
        obj_dict = defaultdict(list)
        map(lambda x_: obj_dict['%s, %s' % (x_['movie_id'], x_['position'])].append(x_), obj_list)
        return obj_dict

    posters_height = conf.partials.film_list.posters_height
    # TODO нужно кэширование
    film_list = DbMovie.objects.all().values()[:50]
    bd_movies_count = 0

    countries = get_values_list(DbMovieCountryMap, 'country')
    genres = get_values_list(DbMovieGenreMap, 'genre')
    awards = get_values_list(DbMovieAwardMap, 'award')
    persons = get_person_list()
    resolutions = set()

    for film in film_list:
        film['img'] = film['image'].replace(
            settings.BASE_DIR, "".join([settings.CURRENT_SITE_SCHEME, settings.CURRENT_SITE_HOST])).encode('utf-8')
        k_img = float(posters_height) / film['image_height'] if film['image_height'] else 0
        film['img_height'] = int(film['image_height'] * k_img) if film['image_height'] else 0
        film['img_width'] = int(film['image_width'] * k_img) if film['image_width'] else 0
        film['date_premier_str'] = date2str(film['date_premier'], format_="%d-%m-%Y")
        film['metadata'] = get_movie_metadata(film['path_file'])
        resolutions.add((film['metadata']['width'], film['metadata']['height']))

        film['is_movie'] = os.path.isfile(film['path_file'])
        if film['is_movie']:
            bd_movies_count += 1

        film['is_converted'] = film['path_file'].split('.')[-1] == settings.MOVIE_CONVERTED_EXT

        # TODO вынести в скрипт обновления рейтингов
        # film['kp_rating'], film['imdb_rating'] = get_rating_film(film['id_kinopoisk'])

    return JsonResponse({
        "films": list(film_list),
        "countries": countries,
        "genres": genres,
        "awards": awards,
        "persons": persons,
        "is_admin": is_admin(request.user),
        "new_movies": sorted(get_new_files(is_file_added=False), key=lambda x_: x_['name_file']),
        "resolutions": [{'w': x[0], 'h': x[1]} for x in sorted(resolutions)],
    })


@requires_csrf_token
@admin_role_required
def set_params_film(request):
    u""" Меняет атрибуты у фильма, заданные в словаре values """
    result = {'data': {}}
    params = request.POST.dict()
    values = params.get('values')
    id_ = params.get('film_id')

    try:
        movie = DbMovie.objects.get(id=id_)
    except DbMovie.DoesNotExist:
        result['error'] = u'Объект не найден'
    else:
        for key, val in values.items():
            result['data'][key] = val
            setattr(movie, key, val)
            movie.save()

    return JsonResponse(result)


@requires_csrf_token
@admin_role_required
def load_film_kp(request):
    u""" Загружает фильм с КП по id """
    result = {}
    params = request.POST.dict()
    id_ = params.get('film_id')

    try:
        movie = DbMovie.objects.get(id=id_)
    except DbMovie.DoesNotExist:
        result['error'] = u'Объект не найден'
    else:
        obj = ProxyMovieReload(movie=movie)
        obj.save()

    return JsonResponse(result)


@requires_csrf_token
@admin_role_required
def delete_film(request):
    u""" Удаляет фильм из БД и файл """
    result = {}
    params = request.POST.dict()
    film_id = params.get('film_id')
    delete_file = params.get('delete_file')

    try:
        movie = DbMovie.objects.get(id=film_id)
    except DbMovie.DoesNotExist:
        result['error'] = u'Объект не найден'
    else:
        if delete_file and os.path.isfile(movie.path_file):
            Popen(["rm", movie.path_file])
        movie.delete()

    return JsonResponse(result)


def config_js(request):
    u""" url для передачи в js """
    return render_to_response('config.js', {
        "urls": {
            "get_film_list": reverse(get_film_list),
            "set_params_film": reverse(set_params_film),
            "load_film_kp": reverse(load_film_kp),
            "delete_film": reverse(delete_film),
        },
        "PositionPerson": PositionPerson.key_names,
        "StatusMovie": StatusMovie.names,
        "StatusMovieKeyNames": StatusMovie.key_names,
    })
