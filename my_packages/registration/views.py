# coding=utf-8
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from helpers import send_activation_key
from forms import EMAIL_USED_MESSAGE, USERNAME_USED_MESSAGE
from models import RegistrationProfile


def activate(request, activation_key,
             template_name='activate.html',
             extra_context=None):
    """
    Activate a ``User``'s account from an activation key, if their key
    is valid and hasn't expired.
    
    By default, use the template ``reguser/activate.html``; to
    change this, pass the name of a template as the keyword argument
    ``template_name``.
    
    **Required arguments**
    
    ``activation_key``
       The activation key to validate and use for activating the
       ``User``.
    
    **Optional arguments**
       
    ``extra_context``
        A dictionary of variables to add to the template context. Any
        callable object in this dictionary will be called to produce
        the end result which appears in the context.
    
    ``template_name``
        A custom template to use.
    
    **Context:**
    
    ``account``
        The ``User`` object corresponding to the account, if the
        activation was successful. ``False`` if the activation was not
        successful.
    
    ``expiration_days``
        The number of days for which activation keys stay valid after
        reguser.
    
    Any extra variables supplied in the ``extra_context`` argument
    (see above).
    
    **Template:**
    
    reguser/activate.html or ``template_name`` keyword argument.
    
    """

    # Normalize before trying anything with it.
    activation_key = activation_key.lower()
    account = RegistrationProfile.objects.activate_user(activation_key)
    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    return render_to_response(
        template_name,
        {
            'is_active': account.is_active if account else '',
            'username': account.username if account else ''
        },
        context_instance=context)


def register(request, success_url=None,
             form_class=None, profile_callback=None,
             template_name='reguser/reguser_form.html',
             extra_context=None):
    """
    Allow a new user to reguser an account.
    
    Following successful reguser, issue a redirect; by default,
    this will be whatever URL corresponds to the named URL pattern
    ``registration_complete``, which will be
    ``/accounts/reguser/complete/`` if using the included URLConf. To
    change this, point that named pattern at another URL, or pass your
    preferred URL as the keyword argument ``success_url``.
    
    By default, ``reguser.forms.RegistrationForm`` will be used
    as the reguser form; to change this, pass a different form
    class as the ``form_class`` keyword argument. The form class you
    specify must have a method ``save`` which will create and return
    the new ``User``, and that method must accept the keyword argument
    ``profile_callback`` (see below).
    
    To enable creation of a site-specific user profile object for the
    new user, pass a function which will create the profile object as
    the keyword argument ``profile_callback``. See
    ``RegistrationManager.create_inactive_user`` in the file
    ``reg_models.py`` for details on how to write this function.
    
    By default, use the template
    ``reguser/registration_form.html``; to change this, pass the
    name of a template as the keyword argument ``template_name``.
    
    **Required arguments**
    
    None.
    
    **Optional arguments**
    
    ``form_class``
        The form class to use for reguser.
    
    ``extra_context``
        A dictionary of variables to add to the template context. Any
        callable object in this dictionary will be called to produce
        the end result which appears in the context.
    
    ``profile_callback``
        A function which will be used to create a site-specific
        profile instance for the new ``User``.
    
    ``success_url``
        The URL to redirect to on successful reguser.
    
    ``template_name``
        A custom template to use.
    
    **Context:**
    
    ``form``
        The reguser form.
    
    Any extra variables supplied in the ``extra_context`` argument
    (see above).
    
    **Template:**
    
    reguser/reguser_form.html or ``template_name`` keyword
    argument.
    
    """
    def if_forgotten_activate(_form):
        u""" проверка потерянной активации
        """
        if (u'email' in _form.errors.keys() and EMAIL_USED_MESSAGE in _form.errors[u'email'] and
           u'username' in _form.errors.keys() and USERNAME_USED_MESSAGE in _form.errors[u'username']):
            user = User.objects.get(email=_form.data[u'email'], username=_form.data[u'username'])
            if user.is_active is False:
                try:
                    profile = RegistrationProfile.objects.get(user=user)
                except RegistrationProfile.DoesNotExist:
                    profile = None
                send_activation_key(user, profile)

            return True
        else:
            return False

    if request.method == 'POST':
        form = form_class(request, data=request.POST, files=request.FILES)

        # проверка потерянной активации
        if if_forgotten_activate(form):
            return HttpResponseRedirect(
                success_url or reverse('registration_complete'))

        if form.is_valid():
            form.save(profile_callback=profile_callback)
            # success_url needs to be dynamically generated here; setting a
            # a default value using reverse() will cause circular-import
            # problems with the default URLConf for this application, which
            # imports this file.
            return HttpResponseRedirect(
                success_url or reverse('registration_complete'))
    else:
        form = form_class(request)
    
    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    return render_to_response(
        template_name,
        {'form': form},
        context_instance=context)
