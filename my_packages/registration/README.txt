
-- добавить в settings.py:

# шаблоны
TEMPLATES = [
    {
        'DIRS': [
		    os.path.join(BASE_DIR, 'my_packages/registration/templates').replace('\\', '/')
        ],
    },
]

INSTALLED_APPS = [
    'my_packages.registration',
]

# перенаправление после login
LOGIN_REDIRECT_URL = '/'
# предельный срок активации
ACCOUNT_ACTIVATION_DAYS = 7



-- добавить в urls.py

    url(r'^%s/' % REGUSER_PACKAGE_URL, include('RegUser.urls')),
    url('^home/', accounts_profile, name=RegUser.HOME_URL_NAME_SPACE),
