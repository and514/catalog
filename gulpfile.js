var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    jade = require('gulp-jade'), // Плагин для Jade
    stylus = require('gulp-stylus'), // Плагин для Stylus
    livereload = require('gulp-livereload'), // Livereload для Gulp
    myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
    csso = require('gulp-csso'), // Минификация CSS
    cssmin = require('gulp-minify-css'), // Минификация CSS
    imagemin = require('gulp-imagemin'), // Минификация изображений
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    connect = require('connect'), // Webserver
    server = lr();


// Собираем JS
//        .pipe(uglify()) // минимизируем
gulp.task('js', function() {
    gulp.src([
        'bower_components/angular/angular.min.js',
        'bower_components/angular-resource/angular-resource.min.js',
        //'bower_components/angular-route/angular-route.js',
        'bower_components/angular-ui-router/release/angular-ui-router.min.js',
        'bower_components/angular-cookies/angular-cookies.min.js',
        'bower_components/angular-xeditable/dist/js/xeditable.min.js',
        //'bower_components/angular-animate/angular-animate.min.js',
        'bower_components/underscore/underscore-min.js',
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/angular-sanitize/angular-sanitize.min.js',
        // 'bower_components/angular-bootstrap/ui-bootstrap-min.js',
        // 'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        'src/catalog/front/js/*.js'
    ])
        .pipe(concat('app.js')) // Собираем все JS
        .pipe(gulp.dest('static/js'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});

// Собираем css
gulp.task('css', function() {
    gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
        'bower_components/angular-xeditable/dist/css/xeditable.css',
        'src/catalog/front/css/style.css'
    ])
        .pipe(concat('style.css')) // Собираем все JS
        .pipe(gulp.dest('static/css'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});


gulp.task('default', ['js', 'css']);