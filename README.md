```bash
sudo apt-get install nginx python-django sqlite3 libsqlite3-dev
sudo pip install future simplejson python-imaging Pillow pyyaml bs4 requests BeautifulSoup libxml2-dev libxslt1-dev lxml supervisor
```

## свои либы
sudo pip install --user lib_packages/core/dist/lib_packages_core-0.1.tar.gz

## для кодирования видео
sudo add-apt-repository ppa:mc3man/trusty-media && sudo apt-get update
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8E51A6D660CD88D67D65221D90BD7EACED8E640A
sudo apt-get install ffmpeg

## avi metadata
sudo pip install hachoir-core hachoir-parser hachoir-metadata


## nginx
sudo ln -s ~/Projects/catalog/configs/catalog_nginx_home.conf /etc/nginx/sites-enabled/
sudo ln -s ~/Projects/catalog/configs/catalog_nginx_work.conf /etc/nginx/sites-enabled/
sudo nginx -s reload

##supervisor
sudo ln -s ~/Projects/catalog/configs/catalog_supervisor.conf /etc/supervisor/conf.d/

## hosts
sudo gedit /etc/hosts
0.0.0.0       local.catalog.com

##запускать сервер
python manage.py runserver 0.0.0.0:8001

##logs
sudo mkdir /var/log/www & sudo chown -R www-data:www-data /var/log/www & sudo chmod 777 -R

## npm, gulp
sudo apt-get install nodejs npm build-essential
sudo npm install gulp -g
## sudo npm init - если в первый раз
sudo npm install gulp gulp-jade gulp-stylus gulp-livereload gulp-myth gulp-csso gulp-imagemin gulp-uglify gulp-concat gulp-minify-css tiny-lr connect --save-dev

## bower
sudo npm install -g bower
sudo npm install -g bower-update
bower install

## если в первый раз
## bower init
## ставим пакеты, если нужно
## bower install --save underscore
## обновляем библиотеки
## bower-update


- миграции
python manage_catalog.py migrate

## перекодировать видео в ogg (вручную)
## ffmpeg2theora -v 10 --optimize -a 10 -o film.ogv /media/and/Seagate Expansion Drive/media/кино/Ванильное небо 2001.mkv
ffmpeg2theora -v 10 --optimize -a 10 -o film.ogv /media/and/Seagate\ Expansion\ Drive/media/кино/Ванильное\ небо\ 2001.mkv


СКРИПТЫ:

## назначить суперпользователя (django)
python manage_catalog.py createsuperuser --username USERNAME --email EMAIL --database DATABASE
## изменить пароль у пользователя
django-admin changepassword USERNAME --database DATABASE

## закачать фильмы в БД
python manage_catalog.py add_films_kinopoisk -p "/home/akorovin/test-catalog"
python manage_catalog.py add_films_kinopoisk -p "/home/and/test-movies"
python manage_catalog.py add_films_kinopoisk -p "/media/and/Seagate Expansion Drive/media/кино"
python manage_catalog.py add_films_kinopoisk -p "/home/and/1 Downloads/кино"


## crontab -e  (work)
```text
*/15 * * * * python /home/akorovin/Projects/catalog/manage_catalog.py convert_films >> /var/log/www/001_log_convert_1con.txt 2>> /var/log/www/002_log_convert_2err.txt
*/30 * * * * python /home/akorovin/Projects/catalog/manage_catalog.py add_films_kinopoisk >> /var/log/www/001_log_add_kinopoisk_1con.txt 2>> /var/log/www/002_log_add_kinopoisk_2err.txt
```

## crontab -e  (home)
```textmate
*/15 * * * * python /home/and/Projects/catalog/manage_catalog.py convert_films >> /var/log/www/001_log_convert_1con.txt 2>> /var/log/www/002_log_convert_2err.txt
*/30 * * * * python /home/and/Projects/catalog/manage_catalog.py add_films_kinopoisk >> /var/log/www/001_log_add_kinopoisk_1con.txt 2>> /var/log/www/002_log_add_kinopoisk_2err.txt
```
## log - /var/log/syslog
